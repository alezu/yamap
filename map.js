(function ($) {

  var myMap;

    ymaps.ready(init);
    function init() {
      var ht = Drupal.settings.ya_height;
      var wd = Drupal.settings.ya_width;
      var ya_zoom = Drupal.settings.ya_zoom;
      var point = Drupal.settings.ya_coords.split(',');

      $('yamap').css({height: ht, width: wd});
      myMap = new ymaps.Map('yamap', {
        center: point,
        zoom: ya_zoom
      });

      myMap.controls
              .add('zoomControl', {left: 5, top: 5})
              .add('typeSelector')
              .add('mapTools', {left: 35, top: 5});
      var Placemark = new ymaps.Placemark(point, {hintContent: 'ОАО «Краснодартеплосеть»'}, {
          iconLayout: 'default#image',
          // iconImageHref: '/sites/all/themes/sm/img/map-marker.png',
          // iconImageSize: [50, 76],
          // iconImageOffset: [-25, -76]
        });
      myMap.geoObjects.add(Placemark);
    }

})(jQuery)